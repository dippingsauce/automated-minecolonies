local basalt = require("basalt")
 
 term.clear()
 term.setCursorPos(1,1)
local col = peripheral.find("colonyIntegrator")
if col == nil then
 print("No Colony Integrator Found")
 return
end
 
local bridge = peripheral.find("rsBridge")
if bridge == nil then
 print("No rsBridge Found")
 return
end

builders = {}
data = {}
bi = 1
k = 1
buildings = col.getBuildings()
for i,v in pairs(buildings) do
 builders[bi] = {}
 if v.type == "builder" then
  builders[bi].coords = v.location
  builders[bi].worker = v.citizens[1].name
  bi = bi + 1
 end
end
for i,v in pairs(builders) do
 if v.coords then
  coords = v.coords
  data[k] = col.getBuilderResources(coords)
  k = k + 1
 end
end

local function craftItem(arg1)
 sysitems = bridge.getItem({name=arg1.item,count=arg1.needed-arg1.available})
     if bridge.isItemCraftable({name=arg1.item}) == true then
      syspattern = bridge.getPattern({name=arg1.item})
      for i = 1,#syspattern.outputs do
       if sysitems.amount == syspattern.outputs[i].amount then
        sysamount = sysitems.amount - syspattern.outputs[i].amount
       else
        sysamount = sysitems.amount
       end
       if sysamount < (arg1.needed-arg1.available) then
        bridge.craftItem({name=arg1.item,count=(arg1.needed-arg1.available)-sysamount})
        sleep(1)
        while bridge.isItemCrafting({name=arg1.item}) == true do
		       print("Crafting: " .. arg1.item)
        end      
       end
      end
     else
      print(arg1.item .. " can't be crafted")
     end
end

local specialblocks = {}
local m = 1
for i,v in pairs(data) do
 for j,k in pairs(v) do
  if k.status == "DONT_HAVE" then
   if k.item:find("domum_ornamentum:", 1, true) then
    specialblocks[m] = k.item
    m = m+1
   else
    craftItem(k)
	   bridge.exportItem({name=k.item,count=k.needed-k.available},"top")
   end
  end
 end
end