# Automated Mine Colonies
This code utilizes the Minecraft Mods CC: Tweaked, Advanced Peripherals, Refined Storage, and Mine Colonies (and maybe some more) in the modpack All The Mods 8 to "automate" the distribution of builder resources.  This SHOULD (in theory) work for any Mine Colony, as long as you place the computer and Colony Integrator (Advanced Peripherals) inside the boundary of the colony. It also SHOULD work for any amount of builders you have in the colony, and will automatically update when adding a new builder.

## Requirements
This code requires you to download [Basalt](https://basalt.madefor.cc/#/) to the computer on your server/world.
You must enable the HTTP API through the CC: Tweaked mod settings for your client/server in order to allow the mod to download from the internet.

## Basalt Installation
Once the HTTP API is enabled, on the CC: Tweaked computer you can run
`wget run https://basalt.madefor.cc/install.lua source basalt-1.6.6.lua`
in the terminal (the minecraft computer terminal, not your actual computer's terminal!) to install basalt.
You MUST download basalt version 1.6.6.  Any newer version will not work.

### Troubleshooting
Sometimes, the installation doesn't format or install properly when using the above instructions. This will become apparent when attempting to run the display.lua file. In this case, it might be best (and potentially easier) to download the basalt.lua file directly from the [github repo](https://github.com/Pyroxenium/Basalt/releases/tag/v1.6.6) and drag and drop it while you are in the CC: Tweaked Computer Terminal (you can do this for display.lua and exportItems.lua, it's the easiest way to put the files onto your Minecraft computer)

## Setup
This code assumes you have power, a crafting grid, a pattern grid, a computer, 9 monitors (not terribly necessary to have all of them, but it's the most 'readable' that way), an RS bridge, a way to transfer items, a colony integrator, and, of course, a colony. 
Place the monitors in a 3x3 grid next to the computer, and the colony integrator and RS bridge in any space where one side will be touching a side of the computer (just not the front). For testing, place a chest on top of the RS bridge. You can replace this with pipes afterwards to transfer items to a warehouse so they will be distributed by a courier to the respective builder(s).

## Running
Once Basalt is installed, and the setup is complete, you can run display.lua to display the UI interface to the monitor you have set up.

## How to Use
On the 'Home' tab, you can see the projects currently being worked on by builders, and a button to send the required resources. You interact with the monitor by right clicking the button / tabs to activate them. 
Clicking the 'Send Required Resources' button will, of course, output the needed resources to the chest / pipe placed above the RS bridge.  Provided you have a pattern grid, the code will also attempt to craft any items required that you do not already have made, so long as you have enough resources in your crafting grid.
There will be a number of other tabs other than 'Home' based on how many builders are currently in your colony. Right clicking these tabs will display the items that builder is currently in need of.

## Extra Output
When running display.lua, clicking 'Send Required Resources' will output any items that are being crafted, as well as items that the pattern grid is unable to craft. Useful to see what you may need to make yourself, or to see if a pattern is not working properly.

## TODO
- Add support for 'special' items, i.e. items that contain 'domum_ornamentum' in their ID
- Move 'Send Required Resources' button from Home tab to each Individual builder's tab. Currently, it sends ALL required resources for EVERY builder.
  - This could be a problem if the courier is slow in delivering the resources to the builder. Clicking the button again after the items are transfered to the warehouse already will craft and / or send any items that the builder has not yet recieved again.
- Fix any additional bugs as they arrive.
