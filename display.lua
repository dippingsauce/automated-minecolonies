local basalt = require("basalt")
 
local col = peripheral.find("colonyIntegrator")
if col == nil then
 print("No Colony Integrator Found")
 return
end
local monitor = peripheral.find("monitor")
monitor.clear()
if monitor == nil then
 print("No Monitor Found")
 return
end
local bridge = peripheral.find("rsBridge")
if bridge == nil then
 print("No rsBridge Found")
 return
end
local monitorName = peripheral.getName(monitor)
 
local main = basalt.createFrame()
 :setMonitor(monitorName)
 :setMonitorScale(0.5)
 :setTheme({FrameBG = colors.white, FrameFG = colors.black})
local sub = {
    main:addFrame():setPosition(1,2)
    :setSize("parent.w","parent.h -1"),
    main:addFrame():setPosition(1,2)
    :setSize("parent.w","parent.h -1"):hide(),
    main:addFrame():setPosition(1,2)
    :setSize("parent.w","parent.h -1"):hide(),
    main:addFrame():setPosition(1,2)
    :setSize("parent.w","parent.h -1"):hide()
}

local button = sub[1]:addButton()
 :setPosition(25,2)
 :setSize(25,3)
 :setText("Send Required Resources")
 :onClick(function(self) 
     shell.run("exportItems.lua")
 end
 )
local labelinprogress = sub[1]:addLabel()
 :setPosition(2,3)
 :setText("Currently Building:")
 :setFontSize(1)
local labelprogress = sub[1]:addLabel() 
 :setText("")
 :setPosition(2,5)
 
local label = {}
for i=1, table.getn(sub)-1, 1 do
    label[i] = sub[i+1]:addLabel()
    :setPosition(2,2)
end
 
local function openSubFrame(id)
    if(sub[id]~=nil)then
        for k,v in pairs(sub)do
            v:hide()
        end
        sub[id]:show()
    end
end
 
menubar = main:addMenubar():setScrollable()
:setSize("parent.w")
:onChange(function(self, val)
openSubFrame(self:getItemIndex())
end)
:addItem("Home")
 
text = ""
texttable = {}
local function printBuilder(...)
    text = ""
    texttable = {}
    for j,k in pairs(arg[1]) do
        if k.status == "DONT_HAVE" then
            pos = string.find(k.item, ":")
            texttable[j] = k.item:sub(pos+1):gsub("_", " ") .. ": " .. k.needed-k.available
        end 
    end
    
    
    for v in pairs(texttable) do
        text = text..texttable[v].."\n"
    end
    label[arg[2]]:setText(text)
end
    
local function programHandler()
    local check = 1
    local inprogress = ""
    while true do 
        local coords = {}
        builders = {}
        data = {}
        local bi = 1
        local k = 1
        local l = 1
        local buildings = col.getBuildings()
            for i,v in pairs(buildings) do
                builders[bi] = {}
                if v.type == "builder" then
                    if v.citizens[1] ~= nil then
                        builders[bi].coords = v.location
                        builders[bi].worker = v.citizens[1].name
                        bi = bi + 1
                    end
                end
                if v.isWorkingOn == true then
                    inprogress = inprogress .. v.type .. "\n"
                    labelprogress:setText(inprogress)
                end
            end
            labelprogress:setText(inprogress)
            inprogress = ""
            for i,v in pairs(builders) do
                if v.coords then
                coords = v.coords
                data[k] = col.getBuilderResources(coords)
                k = k + 1
            end
            
            for j,m in pairs(data) do
                local length = 0
                for k,v in pairs(data) do
                   length = length + 1
                end
            for i = 1,length do
                if check < length+1 then
                    pos = string.find(builders[check].worker, " ")
                    menubar:addItem(string.sub(builders[check].worker, 1, pos))
                    check = check + 1
                end
            end
            for k,v in pairs(m) do
                for g,h in pairs(v) do
                   -- print(g, h)
                end
            end
            printBuilder(m, length)
            end
        end
    end 
end
 
    parallel.waitForAll(basalt.autoUpdate, programHandler)